package Adrian.Gora.Report.generator.view;

import Adrian.Gora.Report.generator.service.FileService;
import Adrian.Gora.Report.generator.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/loadFile")
@RequiredArgsConstructor
@Validated
public class LoadFileController {

    @Autowired
    private FileService fileService;
    @Autowired
    private OrderService orderService;

    @GetMapping
    public ModelAndView orderDtoList(HttpServletRequest request, Model model) {
        if (orderService.getOrderList((String) request.getSession().getAttribute("userId")).isEmpty()) {
            model.addAttribute("message", "You do not have any loaded files");
        } else {
            model.addAttribute("message", "Your files are loaded you can generate your report");
            model.addAttribute("messageType", "success");
        }
        return new ModelAndView("/loadFile");
    }

    @PostMapping
    public ModelAndView addFile(File file, HttpServletRequest request, Model model, HttpServletResponse response) throws IllegalStateException, IOException {

        if (fileService.addFile(file, request, response)) {
            model.addAttribute("message", "Load file is ok, you can now generate your report.");
            model.addAttribute("messageType", "success");
        } else {
            model.addAttribute("message", "Something go wrong, please check yor file and try load it again");
            model.addAttribute("messageType", "danger");
        }
        return new ModelAndView("/loadFile");
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody()
    public ModelAndView conflicts() {

        return new ModelAndView("/loadFile");
    }
}
