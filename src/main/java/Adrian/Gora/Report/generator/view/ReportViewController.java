package Adrian.Gora.Report.generator.view;

import Adrian.Gora.Report.generator.service.OrderService;
import Adrian.Gora.Report.generator.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Controller
@Validated
public class ReportViewController {

    @Autowired
    private ReportService reportService;

    @Autowired
    private OrderService orderService;

    @GetMapping("/home")
    public ModelAndView home() {
        return new ModelAndView("/home");
    }

    @GetMapping("/generalReport")
    public ModelAndView generalReport(Model model, HttpServletRequest request) {

        if (orderService.getOrderList((String) request.getSession().getAttribute("userId")).isEmpty()) {
            return new ModelAndView("/generalReport");
        } else {
            model.addAttribute("allClient", reportService.numberOfAllClient((String) request.getSession().getAttribute("userId")));
            model.addAttribute("allOrders", reportService.numberOfAllOrders((String) request.getSession().getAttribute("userId")));
            model.addAttribute("totalAmount", reportService.totalAmountOfOrders((String) request.getSession().getAttribute("userId")));
            model.addAttribute("average", reportService.averageAmountOfAll((String) request.getSession().getAttribute("userId")));

            return new ModelAndView("/generalReport");
        }
    }

    @GetMapping("/listOfAllClients")
    public ModelAndView listOfAllClients(Model model, HttpServletRequest request) {

        if (reportService.clientList((String) request.getSession().getAttribute("userId")) == null) {
            model.addAttribute("message", "list is empty");
        } else {
            model.addAttribute("clientList", reportService.clientList((String) request.getSession().getAttribute("userId")));
        }
        return new ModelAndView("/listOfAllClients");
    }

    @GetMapping("/listOfAllOrders")
    public ModelAndView listOfAllOrders(Model model, HttpServletRequest request) {

        if (reportService.allOrdersList((String) request.getSession().getAttribute("userId")).isEmpty()) {
            model.addAttribute("message", "list is empty");
        } else {
            model.addAttribute("orderList", reportService.allOrdersList((String) request.getSession().getAttribute("userId")));
        }
        return new ModelAndView("/listOfAllOrders");
    }
}