package Adrian.Gora.Report.generator.view;

import Adrian.Gora.Report.generator.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/orderOfClientPerIndex")
@Validated
public class OrderOfClientPerIndex {

    @Autowired
    private ReportService reportService;

    @GetMapping
    public ModelAndView viewOfClientPerIndex() {
        return new ModelAndView("/orderOfClientPerIndex");
    }

    @PostMapping
    public ModelAndView orderOfClientPerIndex(@Validated @NotNull Integer clientIndex, Model model, HttpServletRequest request) {
        if (!reportService.orderOfClientPerIndex((String) request.getSession().getAttribute("userId"), clientIndex).isEmpty()){
            model.addAttribute("indexOrderList", reportService.orderOfClientPerIndex((String) request.getSession().getAttribute("userId"), clientIndex));
            return new ModelAndView("/orderOfClientPerIndex");
        }else{
            model.addAttribute("message", "invalid client index!");
            return new ModelAndView("/orderOfClientPerIndex");
        }
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody()
    public ModelAndView conflictOrderOfClientPerIndex(Model model) {
        model.addAttribute("message", "invalid client index!");
        return new ModelAndView("/orderOfClientPerIndex");
    }
}
