package Adrian.Gora.Report.generator.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    String id;
    String name;
    String role;
    List<Order> userOrderList;
}
