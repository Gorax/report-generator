package Adrian.Gora.Report.generator.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {

    private int clientId;
    private long requestId;
    private String name;
    private int quantity;
    private double price;
}

