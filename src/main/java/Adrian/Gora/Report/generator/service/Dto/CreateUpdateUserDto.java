package Adrian.Gora.Report.generator.service.Dto;

import Adrian.Gora.Report.generator.model.Order;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateUpdateUserDto {

    String name;
    List<Order> userOrderList;
}