package Adrian.Gora.Report.generator.service.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {

    private int clientId;
    private long requestId;
    private String name;
    private int quantity;
    private double price;
}
