package Adrian.Gora.Report.generator.service.mapper;

import Adrian.Gora.Report.generator.model.Order;
import Adrian.Gora.Report.generator.service.Dto.CreateUpdateOrderDto;
import Adrian.Gora.Report.generator.service.Dto.OrderDto;
import org.springframework.stereotype.Service;

@Service
public class OrderDtoMapper {

    public OrderDto orderToDto(Order order) {
        OrderDto orderDto = new OrderDto(order.getClientId(), order.getRequestId(), order.getName(), order.getQuantity(), order.getPrice());
        return orderDto;
    }

    public Order toModel (CreateUpdateOrderDto createUpdateOrderDto){
        Order order = new Order(createUpdateOrderDto.getClientId(), createUpdateOrderDto.getRequestId(), createUpdateOrderDto.getName(), createUpdateOrderDto.getQuantity(), createUpdateOrderDto.getPrice());
        return order;
    }
}
