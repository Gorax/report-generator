package Adrian.Gora.Report.generator.service;

import Adrian.Gora.Report.generator.model.User;
import Adrian.Gora.Report.generator.service.Dto.CreateUpdateUserDto;
import Adrian.Gora.Report.generator.service.mapper.UserDtoMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserService {

    private List<User> userList = new ArrayList<>();

    private final UserDtoMapper userDtoMapper;

    @PostConstruct
    public void init() {
        userList.add(new User(UUID.randomUUID().toString(), "admin", "admin", new ArrayList<>()));
    }

    public List<User> getUserList() {
        return userList;
    }

    public User addUser(CreateUpdateUserDto createUpdateUserDto, HttpServletRequest request) {
        User userToSaved = userDtoMapper.toModel(createUpdateUserDto);
        request.getSession().setAttribute("userId", userToSaved.getId());
        request.getSession().setMaxInactiveInterval(300);

        userList.add(userToSaved);

        return userToSaved;
    }

    public User findById(String id) {
        return userList.stream().filter(c -> c.getId().equals(id)).findFirst().orElse(null);
    }

    public Boolean existUser(String userName) {
        return userList.stream().filter(c -> c.getName().equals(userName)).findAny().orElse(null) != null ? true : false;
    }

    public User deleteUser(String id) {
        User user = findById(id);
        if (user != null) {
            userList.remove(userList.stream().filter(c -> c.getId().equals(id)).findFirst());
            return user;
        } else {
            return null;
        }
    }
}
