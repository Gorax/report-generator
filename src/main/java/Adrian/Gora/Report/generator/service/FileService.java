package Adrian.Gora.Report.generator.service;

import Adrian.Gora.Report.generator.service.Dto.CreateUpdateOrderDto;
import Adrian.Gora.Report.generator.service.mapper.ReaderCSV;
import Adrian.Gora.Report.generator.service.mapper.ReaderXML;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FileService {

    private final OrderService orderService;

    public boolean addFile(File file, HttpServletRequest request, HttpServletResponse response) throws IOException {
        List<CreateUpdateOrderDto> createUpdateOrderDtoList;

        if (file.getName().endsWith(".csv")) {
            createUpdateOrderDtoList = ReaderCSV.readCsvFile(file, response);
        } else if (file.getName().endsWith(".xml")) {
            createUpdateOrderDtoList = ReaderXML.ReadXmlFile(file);
        } else {
            return false;
        }

        if (createUpdateOrderDtoList != null && !createUpdateOrderDtoList.isEmpty()) {
            orderService.addOrderToUserList(createUpdateOrderDtoList, request);
            return true;
        } else {
            return false;
        }
    }
}
