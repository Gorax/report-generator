package Adrian.Gora.Report.generator.config;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/*")
public class CustomFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String session = (String) request.getSession().getAttribute("userId");
        String requestURI = request.getRequestURI();

        if (
                session != null ||
                        requestURI.endsWith("login") ||
                        requestURI.endsWith(".css")
        ) {
            filterChain.doFilter(request, response);
        } else {
            response.sendRedirect(response.encodeRedirectURL("/login"));
        }
    }

    @Override
    public void destroy() {

    }
}
